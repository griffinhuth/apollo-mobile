import * as firebase from "firebase";

var firebaseConfig = {
  apiKey: "AIzaSyDDoIgi1Q4UK0c1ZW8RVQ3bRH5rfjgsmPE",
  authDomain: "apollo-streams.firebaseapp.com",
  databaseURL: "https://apollo-streams.firebaseio.com",
  projectId: "apollo-streams",
  storageBucket: "apollo-streams.appspot.com",
  messagingSenderId: "531619669919",
  appId: "1:531619669919:web:be8cdbe3ebc1316c6affaa",
  measurementId: "G-CBNGN7LX8Q"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default !firebase.apps.length ? firebase.initializeApp(config) : firebase.app();