
import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { HomeScreen, ProfileScreen } from './Screens';
import { LandingScreen, LoginScreen, RegisterScreen, ForgotPasswordScreen } from './Auth';

export default function App() {
  const Stack = createStackNavigator();
  return (
    <NavigationContainer>
       <Stack.Navigator>
       <Stack.Screen
          name="Apollo Streaming"
          component={LandingScreen}
          options={{headerShown: false}}
        />
       <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="Register"
          component={RegisterScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen 
          name="Profile" 
          component={ProfileScreen} 
          options={{headerShown: false}}
          />    
        <Stack.Screen
          name="Forgot Password"
          component={ForgotPasswordScreen}
          options={{headerShown: false}}
          />
      </Stack.Navigator>
    </NavigationContainer>
  );
}