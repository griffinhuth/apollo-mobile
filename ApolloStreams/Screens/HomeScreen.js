import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  View,
  StyleSheet,
  ScrollView,
  FlatList,
  Text,
  Button
} from 'react-native';

export const HomeScreen = ({navigation}) => {

    const navigateProfile = () => {
        navigation.navigate('Profile');
      };

    return (
     <View>
       <Text>Home Screen</Text>
       <Button
          title="Profile Screen"
          onPress={navigateProfile}
        />
     </View>
    );
  };
  
  const styles = StyleSheet.create({
    icon: {
      width: 42,
      height: 42,
      marginBottom: 80,
      marginLeft: 90,
    },
    contatiner: {
      height: 150,
    },
    topTextViewOne: {
      alignItems: 'center',
      backgroundColor: '#274BDB',
      height: 100,
      flexDirection: 'row',
    },
    topTitleTextOne: {
      color: 'white',
      fontSize: 28,
      marginTop: 50,
      marginLeft: 20,
    },
    topTextViewTwo: {
      alignItems: 'center',
      backgroundColor: '#274BDB',
      height: 80,
      flexDirection: 'row',
    },
    topTitleTextTwo: {
      color: 'white',
      fontSize: 32,
      fontWeight: 'bold',
      marginBottom: 50,
      marginLeft: 20,
    },
    cardView: {
      flex: 1,
      width: '90%',
      marginLeft: 20,
    },
  });
  