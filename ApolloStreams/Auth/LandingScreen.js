import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  View,
  StyleSheet,
  ScrollView,
  FlatList,
  Text,
  Image,
  ImageBackground
} from 'react-native';
import { Button } from 'native-base';

export const LandingScreen = ({navigation}) => {

  const navigateLogin = () => {
    navigation.navigate('Login');
  };
  const navigateRegister = () => {
    navigation.navigate('Register');
  };

return (
 <View>
<View style={{ justifyContent: 'center',
    position: 'relative',}}>

   <ImageBackground
        style={{  position: 'absolute',
    left: 0,
    top: 0,
    bottom:-300,
    right: 0,
    resizeMode: 'cover',
    width: null,
    height: null,}}
        resizeMode='cover'
        source={require('../images/background2.png')}
        blurRadius={1}
      />
 <View style={{justifyContent:'center', alignItems:'center'}}>
 <Image
 style={{width:'90%', marginTop:200, marginBottom:20, height:60}}
        source={
                   require('../images/logowhite.png')}
      />
      <Text style={{color:'white', marginBottom:200, fontSize:48}}>This is your show...</Text>
 </View>
 <View style={{justifyContent:'center', alignItems:'center', alignContent:'center'}}>

   <Button rounded style={{width:'30%', justifyContent:'center', alignItems:'center', alignContent:'center', backgroundColor:'darkgray'}}
      
      onPress={navigateLogin}
    >
    <Text style={{fontSize:20, color:'black', justifyContent:'center', alignItems:'center', alignContent:'center'}}>LOG IN</Text>
    </Button>
    <Button
      rounded style={{width:'30%', justifyContent:'center', alignItems:'center', alignContent:'center', marginTop:20, backgroundColor:'darkgray'}}
      onPress={navigateRegister}
      >
    <Text style={{fontSize:20, color:'black', justifyContent:'center', alignItems:'center', alignContent:'center'}}>CREATE ACCOUNT</Text>
    </Button>
 </View>
</View>
 
 </View>
);
};