import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  StyleSheet,
  ScrollView,
  FlatList,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import { Container, Item, Form, Input, Button, Label, Icon } from "native-base";
// import Icon from 'react-native-vector-icons';
import firebase from '../firebase';

export class LoginScreen extends Component {

  state = ({
    email: '',
    password: ''
  })

    // const navigateHome = () => {
    //     navigation.navigate('Home');
    //   };

      signInUser = (email, password) => {
        try {
          firebase
             .auth()
             .signInWithEmailAndPassword(email, password)
             .then(res => {
                 console.log(res.user.email);
          });
    } catch (error) {
          console.log(error.toString(error));
        }
      };

  render() {
             return (
               <Container style={styles.container}>
                 <ImageBackground
                   style={{
                     position: 'absolute',
                     left: 0,
                     top: 0,
                     bottom: 0,
                     right: 0,
                     resizeMode: 'cover',
                     width: null,
                     height: null,
                   }}
                   resizeMode="cover"
                   source={require('../images/background2.png')}
                   blurRadius={1}
                   shadowColor="black"
                 />
                 <Image
                   style={{
                     width: '90%',
                     marginTop: 130,
                     marginBottom: 20,
                     height: 60,
                   }}
                   source={require('../images/logowhite.png')}
                 />
                 <Text
                   style={{
                     color: 'darkgray',
                     marginBottom: 10,
                     fontSize: 48,
                   }}>
                   This is your show...
                 </Text>
                 <Form style={styles.form}>
                   <Item floatingLabel>
                     <Label style={{fontSize: 25, color:'white'}} >
                     <Icon ios='ios-person' android="md-person" style={{fontSize: 25, color: 'white'}}/>
                     {""} Email Address
                     </Label>
                     <Input
                       style={{height: 65, color: 'white'}}
                       autoCapitalize="none"
                       autoCorrect={false}
                       onChangeText={email => this.setState({email})}
                     />
                   </Item>
                   <Item floatingLabel>
                   
                     <Label style={{fontSize: 25, color:'white'}}>
                     <Icon ios='ios-lock' android="md-lock" style={{fontSize: 25, color: 'white'}}/>
                     {" "}Password
                     </Label>
                     <Input
                       style={{height: 65, color: 'white'}}
                       secureTextEntry={true}
                       autoCapitalize="none"
                       autoCorrect={false}
                       onChangeText={password =>
                         this.setState({password})
                       }
                     />
                   </Item>
                   <Button
                     rounded
                     style={{
                       width: '60%',
                       justifyContent: 'center',
                       alignItems: 'center',
                       alignContent: 'center',
                       marginTop: 30,
                       backgroundColor: 'darkgray',
                     }}
                     onPress={() => {
                       this.signInUser(
                         this.state.email,
                         this.state.password,
                       ),
                         this.props.navigation.navigate('Home');
                     }}>
                     <Text
                       style={{
                         fontSize: 20,
                         color: 'black',
                         justifyContent: 'center',
                         alignItems: 'center',
                         alignContent: 'center',
                       }}>
                       LOG IN
                     </Text>
                   </Button>
                   <TouchableOpacity
                     style={{
                       marginTop: 20,
                     }}
                     onPress={() => {
                       this.props.navigation.navigate(
                         'Forgot Password',
                       );
                     }}>
                     <Text
                       style={{
                         color: 'darkgray',
                       }}>
                       Forgot Password?
                     </Text>
                   </TouchableOpacity>
                 </Form>
               </Container>
             );
           }
 }
 
 const styles = StyleSheet.create({
   container: {
     alignItems: 'center'
  },
  form: {
    width: '35%',
    alignItems: 'center'
   }
 })

