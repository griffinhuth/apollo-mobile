import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  StyleSheet,
  ScrollView,
  FlatList,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import { Container, Item, Form, Input, Button, Label, Icon } from "native-base";

import firebase from '../firebase';

export class RegisterScreen extends Component {

  state = ({
    email: '',
    password: '',
    checkPassword: '',
  })

    // const navigateHome = () => {
    //     navigation.navigate('Home');
    //   };

    signUpUser = (email, password) => {
      try{
        if(this.state.password.length < 6) {
          alert('Please enter at least 6 characters')
          return;
        }
  
        firebase.auth().createUserWithEmailAndPassword(email, password)
      }
      catch(error) {
        console.log(error.toString())
      }
    }

  render() {
    return (
      <Container style={styles.container}>
        <ImageBackground
          style={{
            position: 'absolute',
            left: 0,
            top: 0,
            bottom: 0,
            right: 0,
            resizeMode: 'cover',
            width: null,
            height: null,
          }}
          resizeMode="cover"
          source={require('../images/background2.png')}
          blurRadius={1}
          shadowColor='black'
        />
        <Image
          style={{
            width: '90%',
            marginTop: 130,
            marginBottom: 20,
            height: 60,
          }}
          source={require('../images/logowhite.png')}
        />
        <Text style={{color: 'darkgray', marginBottom: 10, fontSize: 48}}>
          This is your show...
        </Text>
        <Form style={styles.form}>
          <Item floatingLabel>
            <Label style={{fontSize: 25}}>Email</Label>
            <Input
              style={{height: 65, color: 'white'}}
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={email => this.setState({email})}
            />
          </Item>
          <Item floatingLabel>
            <Label style={{fontSize: 25}}>Password</Label>
            <Input
              style={{height: 65, color: 'white'}}
              secureTextEntry={true}
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={password => this.setState({password})}
            />
          </Item>
          <Item floatingLabel>
            <Label style={{fontSize: 25}}>Re Password</Label>
            <Input
              style={{height: 65, color: 'white'}}
              secureTextEntry={true}
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={checkPassword => this.setState({checkPassword})}
            />
          </Item>

          <Button
            rounded
            success
            style={{
              width: '60%',
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
              marginTop: 30,
              backgroundColor: 'darkgray',
            }}
            onPress={() => {
              if (this.state.password !== this.state.checkPassword) {
                alert('Make sure your passwords are identical');
              } else {
                this.signUpUser(this.state.email, this.state.password),
                  this.props.navigation.navigate('Home');
              }
            }}>
            <Text
            style={{
              fontSize: 20,
              color: 'black',
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
            }}>Signup</Text>
          </Button>
        </Form>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center'
 },
 form: {
   width: '35%',
   alignItems: 'center'
  }
})