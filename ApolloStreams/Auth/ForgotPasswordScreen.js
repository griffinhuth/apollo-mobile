import React, { useState } from 'react';
import { Text, View, ImageBackground, Image, StyleSheet } from 'react-native';
import { Container, Item, Form, Input, Button, Label, Icon } from "native-base";

import firebase from '../firebase';

export const ForgotPasswordScreen = ({navigation}) => {

  const [email, setEmail] = useState()

  const auth = firebase.auth();

  const navigateLogin = () => {
    navigation.navigate('Login');
  };

  return (
    <Container style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <ImageBackground
          style={{
            position: 'absolute',
            left: 0,
            top: 0,
            bottom: 0,
            right: 0,
            resizeMode: 'cover',
            width: null,
            height: null,
          }}
          resizeMode="cover"
          source={require('../images/background2.png')}
          blurRadius={1}
          shadowColor='black'
        />
        <Image
          style={{
            width: '90%',
            marginTop: 0,
            marginBottom: 20,
            height: 60,
          }}
          source={require('../images/logowhite.png')}
        />
        <Text style={{color: 'darkgray', marginBottom: 10, fontSize: 48}}>
          This is your show...
        </Text>
        <Form style={styles.form}>
        <Item floatingLabel>
            {/* <Icon name="american-football" /> */}
            <Label style={{fontSize: 25}}>Email Address</Label>
            <Input
              style={{height: 65, color: 'white'}}
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={email => setEmail(email)}
            />
          </Item>
          <Button
            rounded
            style={{
              width: '60%',
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
              marginTop: 30,
              backgroundColor: 'darkgray',
            }}
            onPress={() => {
              auth.sendPasswordResetEmail(email).then(function() {
                // Email sent.
              }).catch(function(error) {
                // An error happened.
              })
            }, navigateLogin}>
            <Text
              style={{
                fontSize: 20,
                color: 'black',
                justifyContent: 'center',
                alignItems: 'center',
                alignContent: 'center',
              }}>
              Send Email
            </Text>
          </Button>
          {/* <Button
            rounded
            style={{
              width: '60%',
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
              marginTop: 30,
              backgroundColor: 'darkgray',
            }}
            onPress={navigateLogin}>
            <Text
              style={{
                fontSize: 20,
                color: 'black',
                justifyContent: 'center',
                alignItems: 'center',
                alignContent: 'center',
              }}>
             Navigate Login
            </Text>
          </Button> */}
          </Form>
    </Container>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center'
 },
 form: {
   width: '35%',
   alignItems: 'center'
  }
})